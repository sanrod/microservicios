#coding: utf-8
# !/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------
# Archivo: sv_sentiments.py
# Tarea: 2 Arquitecturas Micro Servicios.
# Autor(es): Perla Maciel, Jorge Rodriguez, Andres Mitre, Miguel Angel Gonzalez
# Version: 1.0 Mayo 2019
# Descripcion:
#
#   Este archivo define el rol de un servicio. Su funcion general es obtener 
#   la polaridad de un comentario sobre una pelicula, serie o episodio.
#   
#
#
#
#                                        sv_sentiments.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |  - Obtener la polaridad | - Utiliza el API de    |
#           |     Procesador de     |    de los comentarios   |   TextBlob.            |
#           |    sentiminetos de    |    acerca de una pelí-  | - Devuelve un numero   |
#           |      comentarios.     |    cula, serie o        |   entre el -1 y 1,     |
#           |                       |    episodio.            |   siendo el numero >0  |
#           |                       |                         |   Positivo,<0 Negativo |
#           |                       |                         |   =0 Neutral.          |
#           +-----------------------+-------------------------+------------------------+
#
#	Ejemplo de uso: Abrir navegador e ingresar a <Poner ejemplo aquí>
#
import os
from flask import Flask, abort, render_template, request
import urllib, json
import requests
from textblob import TextBlob

app = Flask(__name__)


@app.route("/api/v1/sentiments", methods=["POST"])
def evaluate_sentiments():
    """
    Este método obtiene tweets acerca de una película o serie
    específica.
    :return: JSON con la tweets de la película o serie
    """
    # Se lee el parámetro 't' que contiene el título de la película o serie que se va a consultar

    # number = 100
    # avg = 0

    # for x in xrange(0,number):
    # 	text = unicode(str(tweets[x]), "utf-8")
    # 	sentiments = TextBlob(text)
    # 	avg = avg + sentiments.sentiment[0]

    # avg = avg/number
    text = request.form["text"]
    sentiments = TextBlob(text)
    avg = sentiments.sentiment[0]
    text2 = str(avg)
    return text2



if __name__ == '__main__':
    # Se define el puerto del sistema operativo que utilizará el servicio
    port = int(os.environ.get('PORT', 8082))
    # Se habilita la opción de 'debug' para visualizar los errores
    app.debug = True
    # Se ejecuta el servicio definiendo el host '0.0.0.0' para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port=port)









