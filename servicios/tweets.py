#coding: utf-8
# !/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------
# Archivo: sv_tweets.py
# Tarea: 2 Arquitecturas Micro Servicios.
# Autor(es): Perla Maciel, Jorge Rodriguez, Andres Mitre, Miguel Angel Gonzalez
# Version: 1.0 Mayo 2019
# Descripcion:
#
#   Este archivo define el rol de un servicio. Su funcion general es obtener los tweets acerca de una
#   pelicula, serie o episodio.
#   
#
#
#
#                                        sv_tweets.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |  - Obtener un JSON de   | - Utiliza el API de    |
#           |    Procesador de      |    los comentarios      |   Twitter.             |
#           |    comentarios de     |    acerca de una pelí-  | - Devuelve un JSON con |
#           |       Twitter         |    cula, serie o        |   datos de los comen-  |
#           |                       |    episodio.            |   tarios.              |
#           +-----------------------+-------------------------+------------------------+
#
#	Ejemplo de uso: Abrir navegador e ingresar a <Poner ejemplo aquí>
#
import os
from flask import Flask, abort, render_template, request
import urllib, json
import tweepy
from tweepy import OAuthHandler

app = Flask(__name__)


@app.route("/api/v1/tweets")
def get_tweets():
    """
    Este método obtiene tweets acerca de una película o serie
    específica.
    :return: JSON con la tweets de la película o serie
    """
    # Se lee el parámetro 't' que contiene el título de la película o serie que se va a consultar
    title = request.args.get("t")
    #consumer key, consumer secret, access token, access secret.
    consumer_key = "yo4HTppqMPRNmdFUNt1Dg4ylb"
    consumer_secret = "BdTO0Knlp8qjZNo5VsGvuXuBxPNnMBA5eUAJC344JDmChez6kF"
    access_key = "1123328858596544512-AnPY4GA68ZnbcUNacQIcTedeSlV9pL"
    access_secret = "y98jyWeZt1WHTO5xToogZ1uMOFNz2PrPwzvceO4GWjzsa"

    #Se autentica en twitter
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)

    word_to_search = title + " -filter:retweets"
    tweets = []
    for tweet in tweepy.Cursor(api.search, word_to_search, lang = "en").items(50):
    	try:
    		tweets.append(tweet.text.encode("utf-8"))
    	except tweepy.TweepError as e:
    		print(e.reason)
    	except StopIteration:
    		break
    tweets2 = json.dumps(tweets)
    return tweets2



if __name__ == '__main__':
    # Se define el puerto del sistema operativo que utilizará el servicio
    port = int(os.environ.get('PORT', 8083))
    # Se habilita la opción de 'debug' para visualizar los errores
    app.debug = True
    # Se ejecuta el servicio definiendo el host '0.0.0.0' para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port=port)
